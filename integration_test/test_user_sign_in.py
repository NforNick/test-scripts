import requests
import json
import jwt
from test_data import USER_SIGN_IN_URL


class TestUserSignIn():

    def test_user_sign_in(self):

        data = {
            "username": "nick00404@atrack",
            "password": "00001234",
        }

        response = requests.post(USER_SIGN_IN_URL, json=data)
        token = json.loads(response.text)["data"]["token"]
        content = jwt.decode(token, '0000', algorithms="HS256")

        assert content["data"]["username"] == data["username"]
        assert content["data"]["email"] == json.loads(response.text)[
            "data"]["email"]
        assert response.status_code == 200
