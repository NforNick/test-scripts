import requests
import json
import jwt
from test_data import SEARCH_PRODUCT_URL


class TestSearchProduct():

    def test_search_product(self):
        isbn = "3481592134910"
        category = "fantasy"
        language = "English"
        response = requests.get(SEARCH_PRODUCT_URL, {
                                "isbn": isbn, "category": category, "language": language})
        content = json.loads(response.text)["data"]

        assert content["isbn"] == isbn
        assert content["category"] == [category]
        assert content["language"] == language
        assert response.status_code == 200
