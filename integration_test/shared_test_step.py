import requests
import json
import jwt
from test_data import USER_SIGN_IN_URL, USER_SIGN_UP_URL


def user_sign_in(data):

    response = requests.post(USER_SIGN_IN_URL, json=data)
    token = json.loads(response.text)["data"]["token"]

    return token
