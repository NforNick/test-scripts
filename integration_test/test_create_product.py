import requests
import json
import jwt
from test_data import CREATE_PRODUCT_URL


class TestCreateProduct():
    def test_create_product(self):
        product = {
            "data": {
                "title": "Harry Potter.ver14",
                "author": "Rowling J. K.jr22",
                "language": "English",
                "release_date": "2022-01-07",
                "isbn": "2489131919386",
                "category": "fantasy"
            }


        }
        response = requests.post(CREATE_PRODUCT_URL, json=product)
        content = json.loads(response.text)["data"]

        assert "create_date" or "update_date" in content == True

        assert response.status_code == 201
