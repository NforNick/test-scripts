from django.test import TestCase, Client
import pytest
import requests
import unittest
import json
from django.test import SimpleTestCase
from django.contrib.auth.hashers import make_password
from django.utils import timezone
import datetime
import jwt
from test_data import PRODUCT_COMMENT_URL
from shared_test_step import user_sign_in


class TestProductComment():

    def test_product_comment(self):

        comment = {
            "data": {
                "product_id": 1,
                "user_id": 1,
                "comments": "this is a book",
                "star": 2.5,
                "create_date": "2020-09-23"
            }
        }
        data = {
            "username": "nick00404@atrack",
            "password": "00001234",
        }
        token = user_sign_in(data)
        header = {"AUTHORIZATION": f'Bearer {token}'}

        response = requests.post(
            f'{PRODUCT_COMMENT_URL}{comment["data"]["product_id"]}', headers=header, json=comment)

        assert response.status_code == 200
