import requests
import json
import jwt
from test_data import USER_SIGN_UP_URL


class TestUserSignUp():
    def test_user_sign_up(self):

        data = {
            "username": "102647nick04121404@atrack",
            "password": "00001234",
            "email": "102247qe7021y@atrack.com.tw"}
        response = requests.post(USER_SIGN_UP_URL, json=data)

        token = json.loads(response.text)["data"]["token"]
        content = jwt.decode(token, '0000', algorithms="HS256")
        assert content["data"]["username"] == data["username"]
        assert content["data"]["email"] == data["email"]
        assert response.status_code == 201
