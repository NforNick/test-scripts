import requests
import json
import jwt
from test_data import UPDATE_PRODUCT_URL


class TestUpdateProduct():

    def test_update_product(self):
        product_id = 1
        update_data = {
            "data": {
                "title": "The Saga of Darren22",
                "author": "Darran1 ",
                "language": "Chinese",
                "release_date": "1969-01-15",
                "isbn": "95733348806",
                "category": "romance"
            }
        }
        response = requests.patch(
            f'{UPDATE_PRODUCT_URL}{product_id}', json=update_data)

        content = json.loads(response.text)

        assert {"data": {"message": "Updated successfully."}} == content
        assert response.status_code == 200
