import os

USER_SIGN_UP_URL = 'http://127.0.0.1:8000/api/v1/user/signup'
USER_SIGN_IN_URL = 'http://127.0.0.1:8000/api/v1/user/signin'
USER_PROFILE_URL = 'http://127.0.0.1:8000/api/v1/user/'

CREATE_PRODUCT_URL = 'http://127.0.0.1:8000/api/v1/products/'

SEARCH_PRODUCT_URL = 'http://127.0.0.1:8000/api/v1/products/'

UPDATE_PRODUCT_URL = 'http://127.0.0.1:8000/api/v1/products/'

PRODUCT_DETAIL_URL = 'http://127.0.0.1:8000/api/v1/products/detail/'

PRODUCT_COMMENT_URL = 'http://127.0.0.1:8000/api/v1/products/comment/'
