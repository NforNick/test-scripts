from django.test import TestCase, Client
import pytest
import requests
import unittest
import json
from django.test import SimpleTestCase
from django.contrib.auth.hashers import make_password
from django.utils import timezone
import datetime
import jwt
from test_data import USER_PROFILE_URL
from test_user_sign_in import TestUserSignIn
from shared_test_step import user_sign_in


class TestUserProfile():

    def test_user_profile(self):
        user_id = 1
        data = {
            "username": "nick00404@atrack",
            "password": "00001234",
        }
        token = user_sign_in(data)
        header = {"AUTHORIZATION": f'Bearer {token}'}
        response = requests.get(
            f'{USER_PROFILE_URL}{user_id}', headers=header)
        content = jwt.decode(token, '0000', algorithms="HS256")
        assert json.loads(response.text)[
            "data"]["username"] == content["data"]["username"]
        assert json.loads(response.text)[
            "data"]["email"] == content["data"]["email"]
        assert response.status_code == 200
