from django.test import TestCase, Client
import pytest
import requests
import unittest
import json
from django.test import SimpleTestCase
from django.contrib.auth.hashers import make_password
from django.utils import timezone
import datetime
import jwt
from test_data import PRODUCT_DETAIL_URL


class TestProductDetail():

    def test_product_detail(self):
        product_id = 1
        response = requests.get(f'{PRODUCT_DETAIL_URL}{product_id}')
        content = json.loads(response.text)["data"]
        assert content["id"] == product_id

        assert response.status_code == 200
